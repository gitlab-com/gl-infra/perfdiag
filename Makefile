# make run RUNID=4
# make analysis RUNID=4

RUNID := 0

.PHONY: run
run:
	mkdir -p logs/$(RUNID)
	parallel --results logs/$(RUNID) ./perfdiag.sh $(GCP_PROJECT) $(BUCKET) $(RUNID) {} 20  ::: us-east1-{b,c,d}

.PHONY: run-all
run-all:
	parallel --results logs ./perfdiag.sh $(GCP_PROJECT) $(BUCKET) {1} {2} 3 ::: {1..10}  ::: us-east1-{b,c,d}

.PHONY: clean
clean:
	gcloud --project $(GCP_PROJECT) compute instances list --filter 'name~perfdiag-$(RUNID)' --uri | parallel gcloud --project $(GCP_PROJECT) compute instances delete -q {}

.PHONY: distclean
distclean:
	gcloud --project $(GCP_PROJECT) compute instances list --filter 'name~perfdiag' --uri | parallel gcloud --project $(GCP_PROJECT) compute instances delete -q {}

.PHONY: tail
tail:
	tail -F $(shell find logs -type f)

.PHONY: analyze
analyze:
	ls out/$(RUNID)/perfdiag-$(RUNID)-*.json | parallel --tag "cat {} | jq -r '[.read_throughput.time_took]|@tsv'" | sed -E 's/out\/$(RUNID)\/perfdiag-[0-9]+-us-east1-(.+)\.[0-9]+\.json/\1/g' | datamash --header-out --sort --group 1 count 1 min 2 perc:25 2 perc:50 2 perc:90 2 max 2

.PHONY: analyze-all
analyze-all:
	parallel --tag "cat out/*/perfdiag-*-{}.*.json 2>/dev/null | jq -r '[.read_throughput.time_took]|@tsv'" ::: us-east1-{b,c,d} | datamash --header-out --sort --group 1 count 1 min 2 perc:25 2 perc:50 2 perc:90 2 max 2
