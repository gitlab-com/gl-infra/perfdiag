package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	zone := flag.String("z", "us-east1-d", "zone to analyze")
	flag.Parse()

	connectLatencies := map[string][]float64{}

	err := filepath.WalkDir("out", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		// Check if this is a perfdiag result of a specified zone.
		if !strings.Contains(path, *zone) {
			return nil
		}

		b, err := os.ReadFile(path)
		if err != nil {
			return fmt.Errorf("reading file %s: %v", path, err)
		}

		perfDiag := PerfDiag{}
		json.Unmarshal(b, &perfDiag)

		for key := range perfDiag.Sysinfo.GoogleHostConnectLatencies {
			connectLatencies[key] = append(connectLatencies[key], perfDiag.ReadThroughput.TimeTook)
		}

		return nil
	})
	if err != nil {
		log.Printf("walkDir failed: %v", err)
		os.Exit(1)
	}

	for ip, timeTook := range connectLatencies {
		fmt.Printf("IP: %s\n", ip)
		for _, v := range timeTook {
			fmt.Printf("%f ", v)
		}
		fmt.Println()
		fmt.Println()
	}
}

type PerfDiag struct {
	Sysinfo struct {
		GoogleHostConnectLatencies map[string]interface{} `json:"google_host_connect_latencies"`
	}
	ReadThroughput struct {
		TimeTook float64 `json:"time_took"`
	} `json:"read_throughput"`
}
