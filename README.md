# perfdiag

## Prerequisite

- [`gcloud`](https://cloud.google.com/sdk/docs/install)
- [`make`](https://www.gnu.org/software/make/)
- [`parallel`](https://www.gnu.org/software/parallel/)
- [`datamash`](https://www.gnu.org/software/datamash/)

## Running

Run 1 instance per zone, on a specific bucket.
```shell
# Automatically delete instances on success
make run GCP_PROJECT=gcp-project BUCKET=bucket-name
# Skip deletion of instances
SKIP_CLEANUP=true make run GCP_PROJECT=gcp-project BUCKET=bucket-name
# Delete instances
make clean GCP_PROJECT=gcp-project
```

## Get latencies by host

The `make run` command, populates the `out` directory  with the data from the
`perfdiag` information. Run
[`host_read_throughput.go`](./host_read_throughput.go) to get the time took per
IP address.
