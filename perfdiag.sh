#!/bin/bash

set -x
set -eo pipefail

gcp_project="$1"
bucket="$2"
runid="$3"
zone="$4"
n="$5"

hostname="perfdiag-$runid-$zone"

mkdir -p "out/$runid"

if gcloud --project "$gcp_project" compute instances describe -q "$hostname" --zone "$zone" &>/dev/null; then
  gcloud --project "$gcp_project" compute instances delete -q "$hostname" --zone "$zone"
fi

gcloud --project "$gcp_project" compute instances create "$hostname" --zone "$zone" --image-project ubuntu-os-cloud --image-family ubuntu-2004-lts --machine-type c2-standard-16 --scopes default,storage-full,compute-ro

while ! gcloud --project "$gcp_project" compute ssh "$hostname" --zone "$zone" -- true &>/dev/null; do
  if [[ $SECONDS -gt 300 ]]; then
    >&2 echo "error: timed out after 5 minutes waiting for SSH"
    exit 1
  fi

  sleep 5
done

gcloud --project "$gcp_project" compute ssh "$hostname" --zone "$zone" -- 'sudo apt update -q && sudo apt update -q && sudo apt install net-tools'
for i in $(seq "$n"); do
  gcloud --project "$gcp_project" compute ssh "$hostname" --zone "$zone" -- "gsutil perfdiag -n 1000 -s 80K -t rthru -o output.$i.json gs://$bucket"
  gcloud --project "$gcp_project" compute scp --zone "$zone" "$hostname:output.$i.json" "out/$runid/$hostname.$i.json"
done

if [ -z ${SKIP_CLEANUP+x} ]; then
  gcloud --project "$gcp_project" compute instances delete -q "$hostname" --zone "$zone"
fi
